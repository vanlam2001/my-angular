import { Component } from "@angular/core";

@Component({
    selector: 'app-sample',
    templateUrl: './sample.component.html',
    
})
export class SampleComponent{
    title: string = 'Hello, Angular!';
    count: number = 0;

    increment() {
        this.count++;
    }
}